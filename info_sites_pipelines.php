<?php
/**
 * Définit les pipelines du plugin Info Sites
 *
 * @plugin     Info Sites
 * @copyright  2014-2024
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\Info_Sites\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_liens');
include_spip('base/abstract_sql');
include_spip('inc/actions');
include_spip('inc/autoriser');
include_spip('inc/config');
include_spip('inc/utils');

/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function info_sites_affiche_milieu($flux) {
	$texte = '';
	$e = trouver_objet_exec($flux['args']['exec']);
	$liste_objets = array(
		'organisations',
		'contacts',
		'projets',
		'projets_sites',
	);

	// projets_references sur les projets
	if (is_array($e) and !$e['edition'] and in_array($e['type'], array('projet'))) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'projets_references',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']],
		));
	}

	$liste_plugins = isset($GLOBALS['meta']['plugin']) ? unserialize($GLOBALS['meta']['plugin']) : array();

	// On regarde si le plugin rss_commits est actif.
	if (in_array('rss_commits', $liste_plugins)) {
		$liste_objets[] = 'commits';
	}

	if ($flux['args']['exec'] == "accueil" and $e === false) {
		foreach ($liste_objets as $objet) {
			$texte .= recuperer_fond('prive/objets/liste/' . $objet);
		}
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Insert header prive
 *
 * @param $flux
 *
 * @return string
 */
function info_sites_header_prive($flux) {
	$css = find_in_path('lib/font-awesome/css/font-awesome.min.css');
	$flux .= '<link rel="stylesheet" href="' . $css . '" type="text/css" />';

	return $flux;
}

/**
 * Ajouter les tâches de CRON du plugin Info Sites
 *
 * @param  array $taches Tableau des tâches et leur périodicité en seconde
 *
 * @return array         Tableau des tâches et leur périodicité en seconde
 */
function info_sites_taches_generales_cron($taches) {
	$taches['maj_sites_plugins'] = 24 * 3600; // toutes les 24 heures
	$taches['recuperer_releases'] = 24 * 3600; // toutes les 24 heures

	return $taches;
}


function info_sites_compiler_branches_logiciel($flux) {
	if (is_array($flux['data']) and count($flux['data'])) {
		if (isset($flux['data']['drupal']) and count($flux['data']['drupal'])) {
			foreach ($flux['data']['drupal'] as $index => $branche) {
				$flux['data']['drupal'][$index] = strval(intval($branche));
			}
			$flux['data']['drupal'] = array_unique($flux['data']['drupal']);
		}
		if (isset($flux['data']['wordpress']) and count($flux['data']['wordpress'])) {
			foreach ($flux['data']['wordpress'] as $index => $branche) {
				$flux['data']['wordpress'][$index] = strval(intval($branche));
			}
			$flux['data']['wordpress'] = array_unique($flux['data']['wordpress']);
		}
	}

	return $flux;
}


/**
 * Optimiser la base de données
 *
 * Supprime les liens orphelins de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 *
 * @pipeline optimiser_base_disparus
 *
 * @param  array $flux Données du pipeline
 *
 * @return array       Données du pipeline
 */
function info_sites_optimiser_base_disparus($flux) {
	$flux['data'] += objet_optimiser_liens(array('projets_reference' => '*'), '*');

	return $flux;
}


function info_sites_lister_menu_boutons($boutons) {

	return $boutons;
}

function info_sites_afficher_menu_boutons($flux) {

	$objet = $flux['args']['type'];
	$id_objet = $flux['args']['id'];
	$type_page = $flux['args']['type-page'];
	$composition = $flux['args']['composition'];
	$redirect = $flux['args']['redirect'];
	$qui = $GLOBALS['visiteur_session'];
	
	if ($type_page == 'sommaire') {
		if (autoriser('infositescreer', 'organisation', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				"autorisation" => ['faire' => 'infositescreer', 'type' => 'organisation'],
				'texte' => 'contacts:organisation_creer',
				'lien_url' => generer_url_public('organisation_edit', 'new=oui'),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];	
		}
		if (autoriser('infositescreer', 'organisation', 'new', $qui, array($objet => $id_objet))) {
			$flux['data'][] = [
				'texte' => 'contacts:contact_creer',
				'lien_url' => generer_url_public('contact_edit', 'new=oui'),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositescreer', 'projet', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projet:icone_creer_projet',
				'lien_url' => generer_url_public('projet_edit', 'new=oui'),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositescreer', 'projetscadre', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_cadre:icone_creer_projets_cadre',
				'lien_url' => generer_url_public('projets_cadre_edit', 'new=oui'),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositescreer', 'projetssite', 'new', $qui, array($objet => $id_objet))) {
			$flux['data'][] = [
				'texte' => 'projets_site:icone_creer_projets_site',
				'lien_url' => generer_url_public('projets_site_edit', 'new=oui'),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositescreer', 'projetssite', 'new', $qui, array($objet => $id_objet))) {
			$flux['data'][] = [
				"autorisation" => ['faire' => 'infositescreer', 'type' => 'projetssite'],
				'texte' => 'info_sites:icone_creer_fiche_site',
				'lien_url' => generer_url_public('ajouter_fiche_site'),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
	}
	if ($type_page == 'auteur') {
		if (autoriser('infositesmodifier', 'auteur', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:icone_modifier_auteur',
				'lien_url' => generer_url_public('auteur_edit', 'id_auteur=' . $id_objet . '&redirect=' . $redirect),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositesassocier', 'auteurs', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:auteur_projets_attribuer',
				'lien_url' => generer_url_public('auteur_projets', 'id_auteur=' . $id_objet . '&redirect=' . $redirect),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-muted',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-link'
				]
			];
		}
	}
	if ($type_page == 'contacts') {
		if (autoriser('infositescreer', 'contact', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'contacts:contact_creer',
				'lien_url' => generer_url_public('contact_edit', 'new=oui&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
	}
	if ($type_page == 'contact') {
		if (autoriser('infositesmodifier', 'contact', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:icone_modifier_contact',
				'lien_url' => generer_url_public('contact_edit', 'id_contact=' . $id_objet . '&redirect=' . $redirect),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-primary',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-edit'
				]
			];
		}
		if (autoriser('infositesassocier', 'organisation', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:editer_liens_organisation',
				'lien_url' => generer_url_public('editer_liens', 'table_source=organisations&objet=contact&id_objet=' . $id_objet . '&redirect=' . $redirect),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-muted',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-link'
				]
			];
		}
		if (autoriser('infositessupprimer', 'contact', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'contacts:supprimer_contact',
				'lien_url' => generer_action_auteur('supprimer_contact', 'contact/' . $id_objet, generer_url_public('contacts'), true, '', true),
				'onclick' => "return confirm(\"" . texte_script(_T('contacts:confirmer_supprimer_contact')) . "\n\n" . texte_script(_T('contacts:explication_supprimer_contact')) . "\")",
				'icones' => [
					'stack' => 'text-danger',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-trash'
				]
			];
		}
	}
	if ($type_page == 'organisations') {
		if (autoriser('infositescreer', 'organisation', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'contacts:organisation_creer',
				'lien_url' => generer_url_public('organisation_edit', 'new=oui&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
	}
	if ($type_page == 'organisation') {
		if (autoriser('infositesmodifier', 'oraganisation', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:icone_modifier_organisation',
				'lien_url' => generer_url_public('organisation_edit', 'id_organisation=' . $id_objet . '&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-edit'
				]
			];
		}
		if (autoriser('infositescreer', 'contact', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'contacts:contact_creer',
				'lien_url' => generer_url_public('contact_edit', 'new=oui&associer_objet=organisation|' . $id_objet . '&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositesassocier', 'projet', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:editer_liens_projet',
				'lien_url' => generer_url_public('editer_liens', 'table_source=projets&objet=organisation&id_objet=' . $id_objet . '&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-muted',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-link'
				]
			];
		}
		if (autoriser('infositessupprimer', 'organisation', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'contacts:supprimer_organisation',
				'lien_url' => generer_action_auteur('supprimer_contact', 'organisation/' . $id_objet, generer_url_public('organisations'), true, '', true),
				'icones' => [
					'stack' => 'text-danger',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-trash'
				]
			];
		}
	}

	if ($type_page == 'projets') {
		if (autoriser('infositesmaj', 'projet', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:controle_projets_references_menu',
				'lien_url' => generer_url_public('projets_dashboard', 'controle=projets_references'),
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-eye'
				]
			];
		}
		if (autoriser('infositescreer', 'projet', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projet:icone_creer_projet',
				'lien_url' => generer_url_public('projet_edit', 'new=oui&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
	}
	if ($type_page == 'projet') {
		if (autoriser('infositesmodifier', 'projet', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:icone_modifier_projet',
				'lien_url' => generer_url_public('projet_edit', 'id_projet=' . $id_objet . '&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-edit'
				]
			];
		}
		if (autoriser('maj', 'commits', $id_objet, $qui, array($objet => $id_objet)) and lire_config('rss_commits/import_auto') === 'oui'){
			$flux['data'][] = [
				'texte' => 'commit:maj_commits_projet',
				'lien_url' => generer_action_auteur('maj_commits_projet', $id_objet, $redirect, true, '', true),
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-sync-alt'
				]
			];
		}
		if (autoriser('infositescreer', 'projetssite', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_site:icone_creer_projets_site',
				'lien_url' => generer_url_public('projets_site_edit', 'new=oui&associer_objet=projet|' . $id_objet . '&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositesassocier', 'projetssites', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:editer_liens_projets_site',
				'lien_url' => generer_url_public('editer_liens', 'table_source=projets_sites&objet=projet&id_objet=' . $id_objet),
				'icones' => [
					'stack' => 'text-muted',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-link'
				]
			];
		}
		if (autoriser('infositesassocier', 'contacts', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:editer_liens_contact',
				'lien_url' => generer_url_public('editer_liens', 'table_source=contacts&objet=projet&id_objet=' . $id_objet),
				'icones' => [
					'stack' => 'text-muted',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-link'
				]
			];
		}
		if (autoriser('infositesassocier', 'auteurs', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:editer_liens_auteur',
				'lien_url' => generer_url_public('editer_liens', 'table_source=auteurs&objet=projet&id_objet=' . $id_objet),
				'icones' => [
					'stack' => 'text-muted',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-link'
				]
			];
		}
		// * SELECT 1 FROM `info_sites`.spip_projets_sites_liens AS `L1` WHERE (L1.id_objet = 1) AND (L1.objet = 'projet') LIMIT 1,2
		$site_secu = sql_getfetsel('1', 'spip_projets_sites_liens', "objet='projet' and id_objet=" . $id_objet, '', '', '1,2');
		if ($site_secu and autoriser('infositesmodifier', 'projetssite', $id_objet, $qui, array($objet => $id_objet))){} {
			$flux['data'][] = [
				'texte' => 'info_sites:label_diagnostic_iso',
				'lien_url' => generer_url_public('diagnostic_iso', 'id_projet=' . $id_objet),
				'icones' => [
					'stack' => 'text-primary',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-list'
				]
			];
		}
		if (autoriser('infositessupprimer', 'projet', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projet:supprimer_projet',
				'lien_url' => generer_action_auteur('supprimer_projet', $id_objet, generer_url_public('projets'), true, '', true),
				'onclick' => "return confirm(\"" . texte_script(_T('projet:confirmer_supprimer_projet')) . "\n\n" . texte_script(_T('projet:explication_supprimer_projet')) . "\")",
				'icones' => [
					'stack' => 'text-danger',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-trash'
				]
			];
		}
	}
	if ($type_page == 'projets_cadre') {
		if (autoriser('infositesmodifier', 'projetscadre', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_cadre:icone_modifier_projets_cadre',
				'lien_url' => generer_url_public('projets_cadre_edit', 'id_projets_cadre=' . $id_objet . '&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-edit'
				]
			];
		}
		if (autoriser('infositesassocier', 'projets_cadre', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:editer_liens_auteur',
				'lien_url' => generer_url_public('editer_liens', 'table_source=auteurs&objet=projets_cadre&id_objet=' . $id_objet . '&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-link'
				]
			];
		}
		if (autoriser('infositessupprimer', 'projets_cadre', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_cadre:supprimer_projets_cadre',
				'lien_url' => generer_action_auteur('supprimer_projets_cadre', $id_objet, generer_url_public('projets_cadres'), true, '', true),
				'onclick' => "return confirm(\"" . texte_script(_T('projets_cadre:confirmer_supprimer_projets_cadre')) . "\n\n" . texte_script(_T('projets_cadre:explication_supprimer_projets_cadre')) . "\")",
				'icones' => [
					'stack' => 'text-danger',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-trash'
				]
			];
		}
	}
	if ($type_page == 'projets_cadres') {
		if (autoriser('infositescreer', 'projetscadre', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_cadre:icone_creer_projets_cadre',
				'lien_url' => generer_url_public('projets_cadre_edit', 'new=oui&redirect=' . $redirect),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
	}
	if ($type_page == 'projets_sites') {
		if (autoriser('infositescreer', 'projetssite', 'new', $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_site:icone_creer_projets_site',
				'lien_url' => generer_url_public('projets_site_edit', 'new=oui&redirect=' . $redirect),
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositescreer', 'projetssite', 'new', $qui, array($objet => $id_objet))) {
			$flux['data'][] = [
				"autorisation" => ['faire' => 'infositescreer', 'type' => 'projetssite'],
				'texte' => 'info_sites:icone_creer_fiche_site',
				'lien_url' => generer_url_public('ajouter_fiche_site', 'redirect=' . $redirect),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
	}
	if ($type_page == 'projets_site') {
		if (autoriser('infositesmodifier', 'projetssite', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:icone_modifier_projets_site',
				'lien_url' => generer_url_public('projets_site_edit', 'id_projets_site=' . $id_objet . '&redirect=' . $redirect),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-success',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-plus-circle'
				]
			];
		}
		if (autoriser('infositescreer', 'projetssite', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:icone_cloner_projets_site',
				'lien_url' => generer_action_auteur('cloner_projets_site',  $id_objet, generer_url_public('contacts'), true, '', true),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-copy'
				]
			];
		}
		$webservice = sql_getfetsel('webservice', table_objet_sql($objet), id_table_objet($objet) . '=' . $id_objet . " and webservice <>''");
		if (strlen($webservice) > 1 and autoriser('infositesmaj', 'projetssite', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_site:maj_projets_site',
				'lien_url' => generer_action_auteur('maj_projets_site',  $id_objet, $redirect, true, '', true),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-sync-alt'
				]
			];
			$flux['data'][] = [
				'texte' => 'info_sites:label_webservice_consultation',
				'lien_url' => generer_url_public('webservice', 'id_projets_site=' . $id_objet ),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-globe'
				]
			];
		}
		$logiciels = sql_fetsel('logiciel_plugins, logiciel_nom', table_objet_sql($objet), id_table_objet($objet) . '=' . $id_objet . " and webservice <>''");
		if (strlen($logiciels['logiciel_plugins']) > 1
			and in_array($logiciels['logiciel_nom'], info_sites_lister_diagnostic_logiciel())
			and autoriser('infositesmaj', 'projetssite', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'info_sites:label_diagnostic',
				'lien_url' => generer_url_public('diagnostic', 'id_projets_site=' . $id_objet ),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-primary',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-table'
				]
			];
			$flux['data'][] = [
				'texte' => 'info_sites:label_diagnostic_maj',
				'lien_url' => generer_url_public('diagnostic_maj', 'id_projets_site=' . $id_objet ),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-primary',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-table'
				]
			];
		}
		if (autoriser('infositessupprimer', 'projetssite', $id_objet, $qui, array($objet => $id_objet))){
			$flux['data'][] = [
				'texte' => 'projets_site:supprimer_projets_site',
				'lien_url' => generer_action_auteur('supprimer_projets_site', $id_objet, generer_url_public('projets_sites'), true, '', true),
				'onclick' => "return confirm(\"" . texte_script(_T('projets_site:confirmer_supprimer_projets_site')) . "\n\n" . texte_script(_T('projets_site:supprimer_projets_site_explication')) . "\")",
				'icones' => [
					'stack' => 'text-danger',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-trash'
				]
			];
		}
	} // projets_site
	if ($type_page == 'projets_dashboard') {
		$controles = find_all_in_path('liste/controle/', "\.html$");
		$controles = array_keys($controles);
		foreach ($controles as $controle) {
			$controle = preg_replace('/\.html/', '', $controle);
			$flux['data'][] = [
				'texte' => 'info_sites:controle_' . $controle .'_menu',
				'lien_url' => generer_url_public('projets_dashboard', 'controle=' . $controle ),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-eye'
				]
			];
		}
	} //projets_dashboard
	if ($type_page == 'controle_info_sites') {
		$controles = find_all_in_path('prive/squelettes/inclure/controle/', "\.html$");
		$controles = array_keys($controles);
		foreach ($controles as $controle) {
			$controle = preg_replace('/\.html/', '', $controle);
			$flux['data'][] = [
				'texte' => 'info_sites:controle_' . $controle .'_menu',
				'lien_url' => generer_url_public('controle_info_sites', 'controle=' . $controle ),
				'onclick' => '',
				'icones' => [
					'stack' => 'text-info',
					'icone1' => 'fa-circle',
					'icone2' => 'fa-flag-checkered'
				]
			];
		}
	} //controle_info_sites
	return $flux;
}

function info_sites_complements_zone_aside($flux) {
	$objet = $flux['args']['type'];
	$id_objet = $flux['args']['id'];
	$type_page = $flux['args']['type-page'];
	$composition = $flux['args']['composition'];
	$redirect = $flux['args']['redirect'];

	if ($type_page == 'projet') {
		$flux['data'] .= recuperer_fond('objets/infos/projet', array('id_projet' => $id_objet));
		// * Il faut voir la méthode PHP de SPIP pour insérer un formulaire.
		// if (autoriser('infositesinstituer', 'projet', $id_objet)) {
		// 	$flux .= '<div class="block info">'
		// 	. recuperer_fond('formulaires/instituer_objet', ['projets', $id_objet])
		// 	. '</div>';
		// }
	}

	if ($type_page == 'projets_site') {
		$id_projet = sql_getfetsel('id_objet', 'spip_projets_sites_liens', id_table_objet($objet) . '=' . $id_objet . " and objet='projet'", '', '', '0,1');
		if (!is_null($id_projet)) {
			$flux['data'] .= recuperer_fond('objets/infos/projet', array('id_projet' => $id_projet, 'titre_projet' => 'oui'));
		}
	}

	if ($type_page == 'auteurs') {
		$flux['data'] .= recuperer_fond('objets/infos/auteurs');
	}
	
	return $flux;
}