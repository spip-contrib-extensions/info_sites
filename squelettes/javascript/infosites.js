// on ajoute les titres de section au sommaire du aside pour faire des liens vers les sections

jQuery(document).ready(function ($){
	function infosites_sommaire(target){
		if ($('#aside').length>0 && $('#aside .block.sommaire').length==0){
			$('#aside').append('<div class="block sommaire hidden-sm hidden-xs"><div class="list-group"></div></div>');
		}
		if (target.length>0){
			target.each(function (){
				if ($("#aside .sommaire div.list-group a[href='#"+$(this).attr('id')+"']").length==0){
					$('#aside .sommaire div.list-group').append(
						'<a class="list-group-item" href="#'
						+$(this).attr('id')+'">'
						+$(this).text()
						+'</a>');
				}
			});

		}
	}
	function infosites_form_choix() {
		$(".formulaire_spip form .editer-groupe .editer .choix").each(function(){
			$(this).find('label').addClass('col-form-label').remove('form-label');
			$(this).find('input[type="text"]').addClass('form-control');
			$(this).find('input[type="radio"]').addClass('form-check-input').siblings('label.col-form-label').removeClass('col-form-label form-label').addClass('form-check-label');
			$(this).find('input[type="checkbox"]').addClass('form-check-input').siblings('label.col-form-label').removeClass('col-form-label form-label').addClass('form-check-label');
			$(this).siblings('label').removeClass('form-label').addClass('col-form-label').css({marginRight: '4%'});
		});
	}
	function infosites_formatage(){
		// On active les tooltips pour la sidebar.
		$('#aside a').tooltip({
			placement: 'top',
			trigger: 'hover'
		});
		$('.invisible').each(function(){
			if (!$(this).hasClass('d-none')) {
				$(this).addClass('d-none');
			}
		});

		$('[type=submit]').each(function (event){
			if (!$(this).hasClass('btn')){
				$(this).addClass('btn btn-primary');
			}
		});
		$('.formulaire_instituer [type=submit]').each(function (event){
			$(this).addClass('btn btn-primary');
		});

		$('.actions .editbox').each(function (event){
			if (!$(this).hasClass('btn')){
				$(this).addClass('btn btn-default');
			}
		});

		$('#content .icone.s24 a').each(function (event){
			$(this).addClass('btn btn-default');
		});

		$('.liste-objets.coordonnees .action a').each(function (event){
			if (!$(this).hasClass('btn')){
				$(this).addClass('btn btn-default');
			}
		});

		$('table.spip.liste').each(function (event){
			$(this).addClass('table table-striped table-bordered');
		});

		$('#wysiwyg .champ').each(function(){
			$(this).addClass('row border-bottom');
			$(this).find('> .legend').each(function(){
				$(this).addClass('pt-5 pb-0');
			});
			$(this).find('> .label').each(function(){
				$(this).addClass('col-md-4 pt-3 pb-3');
			});
			$(this).find('> .span').each(function(){
				$(this).addClass('col-md-8 pt-3 pb-3');
			});
			$(this).find('> .label + div > table').parents().siblings('.label').each(function(){
				$(this).removeClass('pb-3');
			});
		});

		var rows = $(".page_diagnostic_iso #content table tr.data");
		rows.each(function (){
			var cells = $(this).find('td');

			for (var i = 1; i<cells.length; i++){
				if (cells.eq(1).html()!=cells.eq(i).html()){
					cells.eq(1).addClass('bg-warning');
					cells.eq(i).addClass('bg-warning');
				}
			}
			$(this).find("table tr td").each(function (){
				$(this).removeClass('bg-warning');
			});
		});

		$('.liste-objets.commits .commit span.titre').click(function (event){
			event.preventDefault();
			var target = $(this);
			var fiche = target.closest('tr').next('.fiche_commit');
			if (fiche.hasClass('hidden')){
				fiche.addClass('visible').removeClass('hidden');
				target.addClass('ouvert').removeClass('ferme');
				target.find('i').addClass('fa-angle-double-down').removeClass('fa-angle-double-right');
			} else if (fiche.hasClass('visible')){
				fiche.addClass('hidden').removeClass('visible');
				target.addClass('ferme').removeClass('ouvert');
				target.find('i').addClass('fa-angle-double-right').removeClass('fa-angle-double-down');
			}
		});
		$('.formulaire_spip form .editer-groupe .editer > *').each(function(){
			infosites_formatage_form($(this));
		});
		$(".formulaire_spip form .editer-groupe .editer.saisie_textarea textarea").each(function(){
			$(this).addClass('form-control');
		});
		$('.formulaire_spip form [type="submit"]').each(function(){
			$(this).removeClass('btn-default').addClass('btn-primary');
		});
	}
	function infosites_formatage_form(element) {
		var aFormTags = ['button', 'select', 'option', 'optgroup', 'fieldset', 'label', 'output'], aFormControl = ['input', 'textarea'], aTagsText = ['p', 'div', 'span'], aTypeCheck = ['radio', 'checkbox'];
		var sElementTagName = element[0].tagName.toLowerCase();
		if (aFormTags.includes(sElementTagName)) {
			element.addClass('form-' + sElementTagName);
		} else if (aTagsText.includes(sElementTagName)) {
			if (element.hasClass('choix') || element.hasClass('edition') || element.hasClass('show')) {
				element.removeClass('form-text');
				element.find(' > *').each(function(){
					infosites_formatage_form($(this));
				});
			} else {
				element.addClass('form-text');
			}
		} else if (aFormControl.includes(sElementTagName)) {
			// console.log(element);
			if (aTypeCheck.includes(element.attr('type'))) {
				element.addClass('form-check-input');
			} else {
				element.addClass('form-control');
			}
		}
	}

	infosites_formatage();
	infosites_form_choix();
	var current_href = window.location.pathname + window.location.search;
	current_href = current_href.replace('/', '');
	$('a[href]').each(function (){
		if ($(this).attr('href') == current_href) {
			$(this).addClass('active');
		}
	});

	$(document).on('ajaxComplete', function(){
		infosites_sommaire($('.contenu .legend'));
		infosites_sommaire($('#extra .legend'));
		infosites_formatage();
		infosites_form_choix();
	});
});
