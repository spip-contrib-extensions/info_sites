<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_fiche_site_rapide' => 'Ajouter rapidement un site',
	'ajouter_projets_rapide' => 'Ajouter des projets',
	'ajouter_projets_rapide_explication' => 'Cette page vous permet d’ajouter rapidement des projets en base de données, à partir de leur url en ligne. Le site de votre projet doit être accessible en ligne car le formulaire récupérera le &lt;title&gt; de la page d’accueil comme nom de projet.',
	'architecte_label' => 'Architecte',
	'auteur_commits_label' => 'Commits de l’utilisateur',
	'auteur_contacts_label' => 'Contacts de l’utilisateur',
	'auteur_identifie_informations' => 'Vos informations',
	'auteur_identifie_profil' => 'Votre profil',
	'auteur_organisations_label' => 'Organisations de l’utilisateur',
	'auteur_projets_attribuer' => 'Attribuer des projets à cet utilisateur',
	'auteur_projets_cadres_label' => 'Cadres de projets de l’utilisateur',
	'auteur_projets_label' => 'Projets de l’utilisateur',
	'auteur_projets_sites_label' => 'Sites de l’utilisateur',
	'auteur_vos_commits_label' => 'Vos commits',
	'auteur_vos_contacts_label' => 'Vos contacts',
	'auteur_vos_adresses_label' => 'Vos adresses',
	'auteur_vos_messages_label' => 'Vos messages',
	'auteur_vos_organisations_label' => 'Vos organisations',
	'auteur_vos_projets_cadres_label' => 'Vos cadres de projets',
	'auteur_vos_projets_label' => 'Vos projets',
	'auteur_vos_projets_references_label' => 'Vos références de projets',
	'auteur_vos_projets_sites_label' => 'Vos sites',
	'auteur_vos_projets_sites_maj_label' => 'Vos sites à mettre à jour',
	'autres_label' => 'Autres',

	// B
	'btn_label_actions' => 'Actions',
	'btn_telecharger' => 'Télécharger',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'champ_identifiant_explication' => 'Identifiant unique du projet permettant de l’identifier rapidement dans les différents outils de gestion du projet. <em>Exemples : gestion administrative, service financier, GED, bugs tracker, etc.</em>',
	'champ_identifiant_label' => 'Identifiant unique',
	'champ_url_bug_tracker_label' => 'URL du gestionnaire de bugs',
	'champ_url_ged_explication' => 'URL de la Gestion Électronique des Documents',
	'champ_url_ged_label' => 'URL de la GED',
	'chef_projets_label' => 'Chef de projets',
	'commercial_label' => 'Commercial',
	'confirmer_cloner_projets_site' => 'Êtes-vous sûr de vouloir cloner ce site ?',
	'contact_existant' => 'Ce contact existe déjà.',
	'controle_auteurs_menu' => 'Les utilisateurs',
	'controle_auteurs_projets_orphelins' => 'Utilisateurs sans projets',
	'controle_auteurs_projets_orphelins_explication' => 'Les utilisateurs listés ci-dessous n’ont pas encore été rattaché à un projet. Merci d’y remédier le cas échéant.',
	'controle_auteurs_titre_page' => 'Les utilisateurs',
	'controle_contacts_lies_aux_projets' => 'Est-ce que les contacts sont liés aux projets&nbsp;?',
	'controle_contacts_menu' => 'Les contacts',
	'controle_contacts_orphelins' => 'Contacts sans organisation',
	'controle_contacts_orphelins_organisations' => 'Contacts sans organisations',
	'controle_contacts_orphelins_projets' => 'Contacts sans projets',
	'controle_contacts_titre_page' => 'Quelques points de contrôle sur les contacts',
	'controle_coordonnees_liees_contacts' => 'Est-ce que les coordonnées sont activées pour les contacts&nbsp;?',
	'controle_explication' => 'Veuillez sélectionner un point de contrôle dans le menu.',
	'controle_extension_curl_actif' => 'Est-ce que l’extension cURL de PHP est activée&nbsp;?',
	'controle_info_sites_menu_auteurs' => 'Est-ce que l’entrée <strong>"Utilisateurs"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_info_sites_menu_commits' => 'Est-ce que l’entrée <strong>"Commits"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_info_sites_menu_contacts' => 'Est-ce que l’entrée <strong>"Contacts"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_info_sites_menu_organisations' => 'Est-ce que l’entrée <strong>"Organisations"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_info_sites_menu_projets' => 'Est-ce que l’entrée <strong>"Projets"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_info_sites_menu_projets_cadres' => 'Est-ce que l’entrée <strong>"Cadres de projet"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_info_sites_menu_projets_sites' => 'Est-ce que l’entrée <strong>"Sites"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_info_sites_menu_statistiques' => 'Est-ce que l’entrée <strong>"Statistiques"</strong> est présente dans le menu de navigation&nbsp;?',
	'controle_ok' => 'Point de contrôle réussi',
	'controle_organisations_menu' => 'Les organisations',
	'controle_organisations_orphelins_auteurs' => 'Organisations sans utilisateurs',
	'controle_organisations_orphelins_contacts' => 'Organisations sans contacts',
	'controle_organisations_orphelins_projets' => 'Organisations sans projets',
	'controle_organisations_titre_page' => 'Quelques points de contrôle sur les organisations',
	'controle_projets_auteurs_orphelins' => 'Projets sans utilisateurs',
	'controle_projets_auteurs_orphelins_explication' => 'Les projets qui seraient listés ci-dessous sont sans utilisateurs associés.',
	'controle_projets_references_menu' => 'Références de projets',
	'controle_projets_date_debut_vide' => 'Date de démarrage non-saisie',
	'controle_projets_date_livraison_prevue_vide' => 'Date de livraison prévue, non-saisie',
	'controle_projets_date_livraison_vide' => 'Date de livraison effective, non-saisie',
	'controle_projets_lies_organisations' => 'Est-ce que les projets sont liés aux organisations&nbsp;?',
	'controle_projets_menu' => 'Les projets',
	'controle_projets_organisations_orphelins' => 'Projets sans organisations',
	'controle_projets_organisations_orphelins_explication' => 'Les projets listés ci-dessous ne sont liés à aucune organisation.',
	'controle_projets_orphelins' => 'Projets sans organisation',
	'controle_projets_orphelins_sites' => 'Projets sans sites liés',
	'controle_projets_projets_sites_menu' => 'Les projets et leurs sites',
	'controle_projets_sites_logiciel_nom_vide' => 'Nom du logiciel non-renseigné',
	'controle_projets_sites_logiciel_version_vide' => 'Version du logiciel non-renseigné',
	'controle_projets_sites_menu' => 'Les sites de projets',
	'controle_projets_sites_orphelins' => 'Sites sans projet parent',
	'controle_projets_sites_titre_page' => 'Quelques points de contrôle sur les sites de projets',
	'controle_projets_sites_uniqid_vide' => 'Unique ID non-renseigné',
	'controle_projets_sites_webservice_vide' => 'URL de webservice non-renseigné',
	'controle_projets_titre_page' => 'Quelques points de contrôle sur les projets',
	'controle_projets_url_site_vide' => 'Url du projet non-renseigné',
	'controle_projets_versioning_path_vide' => 'Chemin du dépôt non-renseigné',
	'controle_projets_versioning_rss_vide' => 'RSS des commits du dépôt non-renseigné',
	'controle_projets_versioning_trac_vide' => 'Trac du dépôt non-renseigné',
	'controle_projets_versioning_type_vide' => 'Type de versioning non-renseigné',
	'controle_roles_auteurs_projets_explication' => 'Vérifier que tous les utilisateurs associés à un projet ont bien un rôle défini. Les utilisateurs qui seraient listés dans le tableau ci-dessous n’ont pas de rôle défini sur le projet auquel ils sont liés. Il faudra y remédier le cas échéant.',
	'controle_roles_auteurs_projets_ok' => 'Tous les utilisateurs ont au moins un rôle défini sur les projets auxquels ils sont associés.',
	'controle_roles_auteurs_projets_orphelins' => 'Utilisateurs sans rôle sur leur projet',
	'creation_rapide_fiche' => 'Fiche condensée',

	// D
	'developpeur_label' => 'Développeur',
	'diagnostic_logiciel_absent' => 'Il n’y a pas de protocole de diagnostic défini pour ce logiciel.',
	'dir_projets_label' => 'Directeur de projets',

	// E
	'editer_liens_auteur' => 'Joindre un utilisateur',
	'editer_liens_contact' => 'Joindre un contact',
	'editer_liens_organisation' => 'Joindre une organisation',
	'editer_liens_projet' => 'Joindre un projet',
	'editer_liens_projets_site' => 'Joindre un site',
	'editer_liens_projets_site_title' => 'Associer une fiche existante de site.',

	// F
	'fieldset_legend_adresse' => 'Adresse',
	'fieldset_legend_contact' => 'Contact',
	'fieldset_legend_email' => 'Courriel',
	'fieldset_legend_numero' => 'Numéro de téléphone',
	'filtres_label' => 'Filtres',

	// I
	'icone_cloner_projets_site' => 'Cloner ce site',
	'icone_creer_fiche_site' => 'Créer une fiche de site',
	'icone_modifier_auteur' => 'Modifier cet utilisateur',
	'icone_modifier_contact' => 'Modifier ce contact',
	'icone_modifier_organisation' => 'Modifier cette organisation',
	'icone_modifier_projet' => 'Modifier ce projet',
	'icone_modifier_projets_cadre' => 'Modifier ce cadre de projet',
	'icone_modifier_projets_site' => 'Modifier ce site',
	'info_1_auteur' => 'Un utilisateur',
	'info_1_contact' => 'Un contact',
	'info_1_organisation' => 'Une organisation',
	'info_1_plugin_maj' => 'Un plugin à mettre à jour',
	'info_1_projet' => 'Un projet',
	'info_1_projets_site' => 'Un site',
	'info_auteurs' => 'Les utilisateurs',
	'info_critere_sans_webservice' => 'Sans URL de webservice',
	'info_nb_auteurs' => '@nb@ utilisateurs',
	'info_nb_contacts' => '@nb@ contacts',
	'info_nb_organisations' => '@nb@ organisations',
	'info_nb_plugin_maj' => '@nb@ plugins à mettre à jour',
	'info_nb_projets' => '@nb@ projets',
	'info_nb_projets_sites' => '@nb@ sites',
	'info_nombre_contacts' => 'Contacts liés',
	'info_nombre_projets' => 'Nombre de projets',
	'info_sites_titre' => 'Info Sites',
	'info_vos_criteres' => 'Vos critères',
	'integrateur_label' => 'Intégrateur',

	// L
	'label_autres' => 'Autres',
	'label_branche' => 'Branche',
	'label_branche_version' => 'Version',
	'label_controle' => 'Points de contrôle',
	'label_diagnostic' => 'Diagnostic',
	'label_diagnostic_iso' => 'Diagnostic ISO',
	'label_diagnostic_maj' => 'Mettre à jour les plugins',
	'label_nom_prenom' => 'Nom, Prénom',
	'label_selectionner_site' => 'Sélectionner un site&nbsp;:',
	'label_stats_general' => 'Statistiques générales',
	'label_total' => 'Total',
	'label_type_site' => 'Environnements',
	'label_valeur' => 'Valeur',
	'label_validation' => 'Validation',
	'label_webservice_consultation' => 'Consulter le webservice',
	'lead_developpeur_label' => 'Lead Développeur',
	'liste_url_projets_explication' => 'Veuillez saisir ci-dessous la liste des urls de vos projets à ajouter. Une url par ligne.',
	'liste_url_projets_label' => 'L’url de projets en ligne',

	// M
	'menu_auteurs' => 'Utilisateurs',
	'menu_commits' => 'Commits',
	'menu_contacts' => 'Contacts',
	'menu_organisations' => 'Organisations',
	'menu_projets' => 'Projets',
	'menu_projets_cadres' => 'Cadres de projet',
	'menu_projets_sites' => 'Sites',
	'menu_statistiques' => 'Statistiques',
	'mes_projets_label' => 'Mes projets',

	// N
	'nav_rapide_label' => 'Navigation rapide',
	'non' => 'Non',

	// O
	'oui' => 'Oui',

	// P
	'page_diagnostic' => 'Diagnostic',
	'page_diagnostic_iso' => 'Diagnostic ISO',
	'page_diagnostic_maj' => 'Mise à jour des plugins',
	'parlot_label' => 'Par lot',

	// R
	'ref_tech_label' => 'Référent technique',
	'retour_page_accueil' => 'Revenir à la page d’accueil',

	// S
	'stats_nb_logiciel_version' => 'Nombre de versions du logiciel',
	'stats_nb_versions' => 'Nombre de versions',

	// T
	'techno_label' => 'Technologie',
	'titre_controle_info_sites' => 'Contrôler Info Sites',
	'titre_informations_annexes' => 'Informations annexes',
	'titre_logiciel_plugins_maj' => 'Mise à jour des plugins',
	'titre_logiciel_plugins_maj_meme_branche' => 'Mise à jour des plugins sur la même branche',
	'titre_page_configurer_info_sites' => 'Configurer le plugin Info Sites',
	'titre_page_controle_info_sites' => 'Contrôler le contenu d’Info Sites',
	'titre_page_lister_projets_sites' => 'Lister les sites de projets',
	'titre_page_projets_dashboard' => 'Tableau de bord des projets',

	// V
	'voir_tout' => 'Tout voir',

	// W
	'webservice_consultation' => 'Consultation du webservice',
	'werbservice_inaccessible' => 'Webservice innaccessible',

	// Z
	'zone_restreinte' => 'Vous n’avez pas les droits suffisants pour accéder à cette page.',

);
