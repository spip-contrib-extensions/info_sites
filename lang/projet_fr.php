<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'champ_identifiant_explication' => 'Identifiant unique du projet permettant de l’identifier rapidement dans les différents outils de gestion du projet. <em>Exemples : gestion administrative, service financier, GED, bugs tracker, etc.</em>',
	'champ_identifiant_label' => 'Identifiant unique',
	'champ_url_bug_tracker_label' => 'URL du gestionnaire de bugs',
	'champ_url_ged_explication' => 'URL de la Gestion Électronique des Documents',
	'champ_url_ged_label' => 'URL de la GED',

);
